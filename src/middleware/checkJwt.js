import { AxiosError } from 'axios';
import { axiosIntance } from '../config/config';
import HttpException from '../exceptions/HttpExceptions';
import { parseJwt } from '../util/util';

/**
 *
 * @param {import("express").Request} req
 * @param { import("express").Response} res
 * @param {import("express").NextFunction} next
 */
export function checkJwt(req, res, next) {

    const ambiente = req.params.ambiente || 'cap'
    const authHeader = req.get("Authorization");
    let jwt = '';
    if(!authHeader){
        const httpError = new HttpException(new Error('No hay token valido'), 401)
        next(httpError);
        return
    }
    if(authHeader.startsWith('Bearer ')){
        jwt = authHeader.substring(7, authHeader.length);
    }
    axiosIntance.post("/AUTH_REFRESH_TOKEN/" + ambiente, { jwt })
        .then((response) => {
            let newToken = response?.data?.jwt
            const userToken = parseJwt(newToken);
            if (ambiente.toLocaleLowerCase() === userToken.ambiente) {
                next()
            } else {
                const httpError = new HttpException(new Error('El ambiente del ticket no coincide con el ambiente de la peticion'), 401)
                next(httpError);
            }
        })
        .catch(e => {
            if(e instanceof AxiosError){
                console.log(e.response)
            }
            const httpError = new HttpException(new Error('el token enviado no es valido'), 401)
            next(httpError)
        })



}

