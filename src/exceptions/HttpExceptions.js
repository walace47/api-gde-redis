/**
 *
 *
 * @class HttpException
 */
class HttpException {
    /**
     * @param {Error} error
     * @param {int} status
     * @memberof HttpException
     */
    constructor(error,status){
        this.error = error;
        this.status = status;
    }
}

export default HttpException;
