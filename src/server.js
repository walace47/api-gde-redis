

import { port, redisClient } from './config/config';
import app from './app.js';
import { destroyPools,createConnectionsPools } from './functions/createConnectionPool.js';



const server = app.listen(port, 
    () => {
        console.log(`La aplicacion se ejecuta en el puerto ${port}`)
        /*server.close(() => {
            console.log(`Cerrando servidor`)
            destroyPools()
        })*/
    });



export default server;