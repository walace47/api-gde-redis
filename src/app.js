import express from 'express';
import { errorHandle } from './controllers/errores.js';
import jobs from "./functions/cronJobs.js";
import cors from 'cors';
import router from './routes/index.js';
import { redisClient } from './config/config.js';
import { createConnectionsPools,destroyPools } from './functions/createConnectionPool.js';
import Cleanup from './util/cleanup.js';


const app = express();

app.use(cors());
app.use(express.json())

redisClient.on('error', err => console.log('Redis Client Error', err));
await redisClient.connect();
await createConnectionsPools();
Cleanup(destroyPools)

app.get('/consultas/hello', function (req, res) {
  res.send('Hello World')
})

app.use("/consultas", router)
jobs.start();
app.use(errorHandle);

export default app;