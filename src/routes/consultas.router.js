import express from 'express';
const app = express.Router();
import { ROUTES } from '../config/constantes';
import {
    descargarDocumentoMarcadoController,
    obtenerAdministradorPorReparticion,
    obtenerAuditoriaController,
    obtenerCaratulador,
    obtenerCoreUsuarios, 
    obtenerDetalleExpedientePorNumero, 
    obtenerDetalleExpedientePorNumeroSade, 
    obtenerExpedientes, 
    obtenerExpedientesActivos, 
    obtenerReparticiones,
    obtenerTratas,
    obtenerUsuarioConDNI,
    obtenerUsuariosController,
    obtenerDestinatariosExternosUsuarioController,
    notificarDestinatariosController
} from '../controllers/index';
import { checkIp } from '../middleware/checkIp';
//import { checkJwt } from '../middleware/checkJwt';

app.get(ROUTES.adminPorReparticion, obtenerAdministradorPorReparticion);

app.get(ROUTES.descargarDocumento, descargarDocumentoMarcadoController)

app.get(ROUTES.reparticiones, obtenerReparticiones);

app.get(ROUTES.coreUsuarios, obtenerCoreUsuarios);

app.get(ROUTES.expedientes, [checkIp], obtenerExpedientes);

app.get(ROUTES.tratas, obtenerTratas);

app.get(ROUTES.expedienteDetallePorNumeroSade, [checkIp], obtenerDetalleExpedientePorNumeroSade)

app.get(ROUTES.expedienteDetallePorNumero, obtenerDetalleExpedientePorNumero)

app.get(ROUTES.caratuladores, obtenerCaratulador)

app.get(ROUTES.obtenerUsuarioDNI, obtenerUsuarioConDNI)

app.get(ROUTES.obtenerAuditoria, obtenerAuditoriaController)

app.get(ROUTES.obtenerExpedientesActivos, obtenerExpedientesActivos)

app.get(ROUTES.usuarios,[checkIp], obtenerUsuariosController)

//app.get(ROUTES.obtenerComunicadosUsuario,[checkJwt], obtenerDestinatariosExternosUsuarioController)

//app.post(ROUTES.comunicarUsuariosExternos,[checkJwt], notificarDestinatariosController)

export default app;

