import express from 'express';
import ConsultaRouter from './consultas.router';
import ActualizarRouter from './actualizarManual.routes';
const app = express.Router();


app.use(ConsultaRouter);
app.use(ActualizarRouter);

export default app;

