let connection;

class oracledb {

    constructor(config) {
        this.config = config;
        this.data = {
            metaData: [
                { name: "NUMERO" },
                { name: "TIPO_DOCUMENTO" },
                { name: "ANIO" },
                { name: "CODIGO_REPARTICION_ACTUACION" },
                { name: "CODIGO_REPARTICION_USUARIO" },
            ],
            rows: []
        };
    }

    static async getConnection(config) {
        if (!connection) {
            connection = new oracledb(config);
        }
        return connection;
    }

    async execute(query, pars) {
        return this.data;
    }

    async close() {

    }

    save(elem) {
        this.data.rows.push(elem);
    }

    clean() {
        this.data = {
            rows: []
        }
    }
}

module.exports = oracledb;