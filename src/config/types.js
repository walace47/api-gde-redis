/**
 * @typedef {Object} Destinatario
 * @property  {number} id 
 * @property  {string} reparticionDestino 
 * @property  {string} nombreUsuario 
 * @property  {string} mesaExterna 
 * @property  {string} nroSade 
 * @property  {string} leido       
*/

/**
 * @typedef {Object} ParmDestinatarios
 * @property  {string} ambiente 
*/

/**
 * @typedef {Object} UserData
 * @property  {string} reparticion 
 * @property  {string} nombre_apellido 
 * @property  {string} nombre 
 * @property  {string} apellido 
 * @property  {string} cuit 
 * @property  {string} usuario
 * @property  {string} token
*/


/** 
 * @typedef {Object} GDEToken
 * @property {string} sub - nombre de usuario
 * @property {string} ambiente - ambiente para el que es valido el token
 * @property {number} exp - fecha de expiracion del token en nano segundos
 * @property {number} iat - fecha de emision del toke en nano segundos
 */

exports.unused = {};
