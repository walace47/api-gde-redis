import axios from 'axios';
import * as https from 'https';
import * as fs from 'fs';
import HttpException from "../exceptions/HttpExceptions.js";
import { fileURLToPath } from 'url';
import { config } from "dotenv";
import path from 'path'
import { createClient } from "redis";
export const redisClient = createClient();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
config({ path: path.join(__dirname, '../../.env') })

/**
 * Configuracion base de datos oracle datos de GDE
 */
export const oracleDbConfig = Object.freeze({
    user: process.env.PRODUCCION_USUARIO,
    password: process.env.PRODUCCION_PASSWORD,
    connectString: process.env.PRODUCCION_CONNECTSTRING,
    poolIncrement: 0,
    poolMax: 80,
    poolMin: 4,
    poolAlias:'prod'
})

/**
 * Base de datos cap
 */
export const oracleDbConfigCap = Object.freeze({
    user: process.env.CAPACITACION_USUARIO,
    password: process.env.CAPACITACION_PASSWORD,
    connectString: process.env.CAPACITACION_CONNECTSTRING,
    poolIncrement: 0,
    poolMax: 80,
    poolMin: 4,
    poolAlias:'cap'
})

/**
 * Configuracion base de datos oracle usuario de ESCRITURA!
 */
const oracleDbConfigGEDO = Object.freeze({
    user: process.env.PRODUCCION_GEDO_USER,
    password: process.env.PRODUCCION_GEDO_PASSWORD,
    connectString: process.env.PRODUCCION_CONNECTSTRING
})

/**
 * Base de datos cap usuario de ESCRITURA!!!
 */
const oracleDbConfigGEDOCap = Object.freeze({
    user: process.env.CAPACITACION_GEDO_USER,
    password: process.env.CAPACITACION_GEDO_PASSWORD,
    connectString: process.env.CAPACITACION_CONNECTSTRING
})



/**
 * Configuracion redis
 */
export const domRedis = "127.0.0.1:6379";

/**
 * puerto de la aplicacion
 */
export const port = 3004 || process.env.PORT;


export const getConexion = (ambiente) => {
    switch (ambiente.toLowerCase()) {
        case "prod":
            return oracleDbConfig
        case "cap":
            return oracleDbConfigCap
        default:
            throw new HttpException(new Error("Ingrese un ambiente correcto, cap, prod"), 401);
    }
}

export const getConexionEdit = (ambiente) => {
    switch (ambiente.toLowerCase()) {
        case "prod":
            return oracleDbConfigGEDO
        case "cap":
            return oracleDbConfigGEDOCap
        default:
            throw new HttpException(new Error("Ingrese un ambiente correcto, cap, prod"), 401);
    }
}


const httpsAgent = new https.Agent({
    cert: fs.readFileSync(process.env.CRT_XROAD),
    key: fs.readFileSync(process.env.KEY_SSL_XROAD),
    rejectUnauthorized: false
});

export const axiosIntance = axios.create({
    baseURL: process.env.XROAD_BASEURL,
    httpsAgent,
    headers: { 'X-Road-Client': 'OPTIC/GOB/GOB00001/GP-OPTIC' }
})
export const user = {
    username: process.env.MIRIAM_USER,
    password: process.env.MIRIAM_PASSWORD
}
export const solr = process.env.SOLR_PRODUCCION;
