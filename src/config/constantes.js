const PERMISOS = Object.freeze({
    CARATULADOR_INTERNO:"ou=sade.internos,ou=grupos,dc=gob,dc=ar",
    CARATULADOR_EXTERNO:"ou=sade.externos,ou=grupos,dc=gob,dc=ar"
})

const EVENTOS = Object.freeze({
    adminPorReparticion: "admin_por_repaticion",
    reparticiones: "reparticiones",
    reparticionesAux: "reparticionesNew",
    coreUsuarios: "coreUsuarios",
    tratasExpedientes: "tratas_expedientes",
    usuarios: "obtener_usuarios",
    usuario:"obtener_usuario" 
})

const ROUTES = Object.freeze({
    adminPorReparticion: `/${EVENTOS.adminPorReparticion}`,
    reparticiones: `/${EVENTOS.reparticiones}/:ambiente`,
    coreUsuarios: `/${EVENTOS.coreUsuarios}`,
    tratas: `/${EVENTOS.tratasExpedientes}`,
    actualizar: '/actualizar',
    expedientes: '/expedientes',
    caratuladores: '/caratuladores',
    expedienteDetallePorNumero: "/expediente/:anio/:numero",
    expedienteDetallePorNumeroSade: "/expediente/:nroSade",
    obtenerUsuarioDNI: "/usuario/:dni",
    obtenerUltimaReparticionExpediente: "expediente-ultima/:id",
    obtenerAuditoria: "/auditoria/expediente",
    obtenerExpedientesActivos: "/obtenerExpedientesActivos",
    descargarDocumento:'/descargarDocumento/:nroSade',
    usuarios: `/${EVENTOS.usuarios}`,
    obtenerComunicadosUsuario:`/obtenerComunicadosUsuario/:ambiente`,
    comunicarUsuariosExternos:`/comunicarUsuariosExternos/:ambiente`
})

export {
    EVENTOS, PERMISOS, ROUTES
};


