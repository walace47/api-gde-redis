/*const request = require('supertest');
const app = require('../app');
const redis = require('redis');
const { EVENTOS, ROUTES } = require('../config/constantes');
const oracledb = require('oracledb');
const { oracleDbConfig } = require("../config");

let client = redis.createClient();
let connection;

describe('GET /expedientes', () => {
    beforeEach(async () => {
        connection = await oracledb.getConnection(oracleDbConfig);
    });

    afterEach(() => {
        client.clean();
        connection.clean();
    });

    it('deberia obtener informacion en la base de oracle', async done => {
        let valor = [1, "dni", 2021, 2, 3];

        await connection.save(valor);

        let query = {
            fechaDesde: new Date(),
            fechaHasta: new Date(),
            trata: "trata",
            creador: "creador",
            reparticion: "reparticion",
            numero: 123,
            ambiente: "prod"
        }

        request(app)
            .get(`/consultas/${ROUTES.expedientes}`)
            .query(query)
            .expect(200, done)
            .expect((res) => {
                expect(JSON.parse(JSON.stringify(res.body))).toStrictEqual([{
                    "ANIO": valor[2],
                    "CODIGO_REPARTICION_ACTUACION": valor[3],
                    "CODIGO_REPARTICION_USUARIO": valor[4],
                    "NRO_SADE": `${valor[1]}-${valor[2]}-0000000${valor[0]}--${valor[3]}-${valor[4]}`,
                    "NUMERO": valor[0],
                    "TIPO_DOCUMENTO": valor[1],
                }]);
            })
            .expect("Content-Type", /json/)
    });
});*/