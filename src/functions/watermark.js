import {PDFDocument, StandardFonts, rgb, degrees } from 'pdf-lib';

/**
 * 
 * @param {string} pdfBytes pdf en base64 
 * @returns {Promise<string>} pdfBase64
 */
async function addWatermarkToPDF(pdfBytes) {
    const WATERMARK_TEXT = "Documento no valido";
    const TEXT_SIZE = 50;
    //const pdfBytes = await fs.promises.readFile(inputPath,{encoding:"base64"});
    const pdfDoc = await PDFDocument.load(pdfBytes);

    const pages = pdfDoc.getPages();
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

    for (const page of pages) {
        const { width, height } = page.getSize();
        //const textSize = 50;
        
        const textWidth = helveticaFont.widthOfTextAtSize(WATERMARK_TEXT, TEXT_SIZE);
        const textHeight = helveticaFont.heightAtSize(TEXT_SIZE);

        const diagonalRotation = Math.atan2(height, width); // Rotation angle for the diagonal watermark
        const rotationDegrees = diagonalRotation * (180 / Math.PI); // Convert radians to degrees


        const textX = (width - textWidth) / 2;
        const textY = (height - textHeight) / 2;

        page.drawText(WATERMARK_TEXT, {
            x: textX, 
            y: textY, 
            size: TEXT_SIZE,
            font: helveticaFont,
            color: rgb(0.5, 0.5, 0.5),
            opacity: 0.5,
            rotate: degrees(rotationDegrees)
        });
    }

    const modifiedPdfBytes = await pdfDoc.saveAsBase64({ removeMetadata: true });
    return modifiedPdfBytes;
}

export default addWatermarkToPDF;