import oracledb from 'oracledb';
import { getConexion } from "../../config/config";
import HttpException from '../../exceptions/HttpExceptions';


async function consultarExpedientes({
    fechaDesde = "01/01/2000",
    descripcion,
    anio,
    fechaHasta,
    trata,
    creador,
    reparticion,
    numero,
    offset = 0,
    ambiente = "cap"
}) {
    let connection = null;
    const cantidadRows = 300;
    try {

        if (anio && !Number(anio)) {
            const error = new Error('El campo anio debe ser de tipo numerico ')
            throw new HttpException(error, 400)
        }
        if (numero && !Number(numero)) {
            const error = new Error('El campo numero debe ser de tipo numerico ')
            throw new HttpException(error, 400)
        }
        
        let orden = fechaDesde == '01/01/2000' ? 'DESC' : 'ASC';
        let pars = { fechaDesde, cantidadRows, offset: offset * cantidadRows };

        if (fechaHasta) pars = { ...pars, fechaHasta };
        
        const stringFechaHasta = fechaHasta ? "TO_DATE(:fechaHasta, 'DD/MM/YYYY' ) + 1 " : "SYSDATE";
        let whereClausure = `WHERE ee.fecha_creacion BETWEEN (TO_DATE(:fechaDesde,'DD/MM/YYYY') ) and ${stringFechaHasta}`;

        if (trata) {
            whereClausure += ` AND t.codigo_trata like :trata `;
            pars = { ...pars, trata };
        }

        if (descripcion) {
            whereClausure += ` AND LOWER(ee.descripcion) like :descripcion `;
            pars = { ...pars, descripcion: descripcion.toLowerCase() };
        }

        if (anio) {
            whereClausure += ` AND ee.anio like :anio `;
            pars = { ...pars, anio };
        }

        if (reparticion) {
            whereClausure += ` AND ee.codigo_reparticion_usuario like :reparticion `;
            pars = { ...pars, reparticion };
        }

        if (creador) {
            whereClausure += ` AND ee.usuario_creador like :creador `;
            pars = { ...pars, creador };
        }

        if (numero) {
            whereClausure += ` AND ee.numero like :numero `;
            pars = { ...pars, numero: Number(numero) };
        }
        const query = `
        SELECT ee.fecha_creacion,ee.fecha_modificacion, ee.descripcion, ee.usuario_creador, ee.tipo_documento, ee.id_workflow,
            ee.anio, ee.numero, ee.codigo_reparticion_actuacion, ee.codigo_reparticion_usuario, ee.solicitud_iniciadora,
            ee.estado, ee.sistema_creador, t.assignee_ as assignee_ee, t.codigo_trata AS codigo_trata
        FROM EE_GED.EE_EXPEDIENTE_ELECTRONICO ee LEFT JOIN EE_GED.JBPM4_TASK t ON ee.ID_WORKFLOW = t.EXECUTION_ID_ INNER JOIN EE_GED.TRATA t ON ee.id_trata = t.id
        ${whereClausure}
        ORDER BY FECHA_CREACION ${orden}
        OFFSET :offset ROWS
        FETCH NEXT :cantidadRows ROWS ONLY`;


        let expedientes = [];
        connection = await oracledb.getPool(ambiente).getConnection();

        let res = await connection.execute(query, pars);



        expedientes = res.rows.map(e => {
            let resp = {};
            res.metaData.forEach((c, i) => resp[c.name] = e[i]);
            resp['NRO_SADE'] = generarNumeroExpediente(resp);
            return resp;
        });
        return expedientes;
    } catch (error) {
        throw error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

/**
 *  * @param {object} datosExpedientes tupla de un expediente
 */
function generarNumeroExpediente(datosExpedientes) {
    const numeroExp = completaZero("00000000" + datosExpedientes.NUMERO, 8);
    let expediente = datosExpedientes.TIPO_DOCUMENTO + '-' + datosExpedientes.ANIO + '-' + numeroExp + '--' + datosExpedientes.CODIGO_REPARTICION_ACTUACION + '-' + datosExpedientes.CODIGO_REPARTICION_USUARIO;
    return expediente;
}

function completaZero(str, chr) {
    return str.substr(str.length - chr, str.length)
}
export default consultarExpedientes
