import oracledb from 'oracledb';
import { getConexion } from "../../config/config";


async function obtenerExpedientesPorHistorial({ areaTramitacion='', reparticionOrigen ,offset = 0 ,fechaDesde = "01/01/2000",fechaHasta, esUltimoPase = false, ambiente = "cap"}) {
    let connection = null;
    const CANTIDAD = 5000;
    let stringFechaHasta;
    let whereReparticionOrigen = '';
    areaTramitacion = areaTramitacion + "%";
    let pars1 = {
        areaTramitacion,
        fechaDesde
    };
    let pars2 = {};
    ambiente = ambiente.toLowerCase();
    if(reparticionOrigen){
        whereReparticionOrigen = `AND b.codigo_reparticion_usuario = :reparticionOrigen`
        pars1 = { ...pars1, reparticionOrigen}
    }
    if(fechaHasta){
        stringFechaHasta = "TO_DATE(:fechaHasta, 'DD/MM/YYYY' ) + 1 ";
        pars1 = {...pars1,fechaHasta};

    } else {stringFechaHasta = 'SYSDATE'};
    pars2 = {...pars1}

    try {
        const whereClausure = `WHERE a.codigo_reparticion_destino LIKE :areaTramitacion
        AND b.Estado NOT LIKE 'Guarda Temporal%' And b.Estado NOT LIKE 'Archivo'
        AND a.Fecha_operacion BETWEEN (TO_DATE(:fechaDesde,'DD/MM/YYYY') ) and ${stringFechaHasta} ${whereReparticionOrigen}`;

        const fromClausure = `FROM  EE_GED.historialoperacion a 
                                        INNER JOIN EE_GED.EE_Expediente_Electronico b ON a.id_expediente = b.id`;
        let countQuery = `SELECT COUNT(*) ${fromClausure} ${whereClausure}`
 
        let query = `
            SELECT a.Expediente, a.Destinatario, a.Fecha_operacion, a.Motivo
            ${fromClausure} 
            ${whereClausure}`;
            

        if(esUltimoPase) {
            const ultimoPase = " AND ord_hist = ( SELECT Max(ord_hist) FROM EE_GED.historialoperacion WHERE Expediente = a.Expediente)"
 
            query = query + ultimoPase             
            countQuery = countQuery + ultimoPase
            
        }
        query = `${query} ORDER BY a.Fecha_operacion desc
                          OFFSET :offset ROWS
                          FETCH NEXT :cantidadRows ROWS ONLY`;
        pars1 = {...pars1, 
            offset: offset * CANTIDAD, 
            cantidadRows:CANTIDAD
        }
        connection = await oracledb.getPool(ambiente).getConnection();


        const query1 = connection.execute(query, pars1);
        const query2 = connection.execute(countQuery, pars2);
        
    
        const [res, count] = await Promise.all([query1,query2])

        const expedientes = res.rows.map(e => {
            const EX =  e[0].substring(0,2);
            const ANIO = e[0].substring(2,6);
            const REPARTICION = e[0].substring(e[0].indexOf('-') + 1);
            let numero = ''
            let ecosistema = '';

            //CAP=PROVINCIA, PROD=NEU
            if( ambiente == 'cap' ){
                numero = e[0].substring(6, e[0].indexOf('-') - 9);
                ecosistema = "PROVINCIA"

            }else if( ambiente == 'prod' ) {
                numero = e[0].substring(6, e[0].indexOf('-') - 3);
                ecosistema = 'NEU'
            }

            
            numero = completaZero("00000000" +  numero, 8);

            const nroSade = EX + "-" + ANIO + "-" + numero + "- -" + ecosistema + "-" +REPARTICION;
            return {
                nroSade,
                destinoActual:e[1],
                fechaOperacion:e[2],
                motivo:e[3]
            }
        })
        const respuestaService = {cantidadTotal: count.rows[0][0], expedientes};

        return respuestaService;
    } catch (error) {
        throw  error ;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw  err;
            }
        }
    }
}

function completaZero(str, chr){
    return str.substr(str.length - chr, str.length)
}




export default  obtenerExpedientesPorHistorial;