import oracledb from 'oracledb';
import { oracleDbConfig, oracleDbConfigCap } from '../config/config';

/**
 *
 *
 * @export
 * @return {Promise<[oracledb.Pool, oracledb.Pool]>} 
 */
export function createConnectionsPools() {
    //pool de conexion prod
    console.log("Creando conexion")
    const p1 = oracledb.createPool(oracleDbConfig);
    //pool de conexion cap
    const p2 = oracledb.createPool(oracleDbConfigCap);
    return Promise.all([p1,p2])
}

export function destroyPools() {
    console.log("destuyendo pools")
    oracledb.getPool('cap').close(10)
    oracledb.getPool('prod').close(10)
}