import { solr } from "../../config/config";
import { EVENTOS } from '../../config/constantes';
import axios from 'axios';
import { redisClient } from '../../config/config';


async function actualizarCoreUsuario() {
    try {
        let url = solr + '/coreUSUARIOS/select?q=*%3A*&version=2.2&start=0&rows=50000&indent=on&wt=json';
        res = await axios(url);
        redisClient.set(EVENTOS.coreUsuarios, JSON.stringify(res.data));
        return res.data;
    } catch (error) {
        throw  error;
    }
}
export default actualizarCoreUsuario;

