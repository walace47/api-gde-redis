import oracledb from 'oracledb';
import { getConexion } from "../../config/config";
import { EVENTOS } from '../../config/constantes';
import { redisClient } from '../../config/config';


/**
 * 
 * @param {string} ambiente 
 * @returns {Promise<import('../../config/types').UserData[]>}
 */
async function actualizarUsuarios(ambiente) {
    let connection = null;
    try {

        const query = `--sql   
        SELECT   
            DISTINCT d.USUARIO, 
                d.NOMBRE, 
                d.APELLIDO, 
                d.NUMERO_CUIT,  
                REP.codigo_reparticion, 
                D.mail, 
                SI.codigo_sector_interno,
                CAR.cargo,
                d.aceptacion_tyc 
            FROM CO_GED.datos_usuario d
                INNER JOIN TRACK_GED.SADE_SECTOR_INTERNO SI ON (d.id_sector_interno = SI.ID_SECTOR_INTERNO or d.id_sector_original = SI.ID_SECTOR_INTERNO)
                INNER JOIN TRACK_GED.SADE_REPARTICION REP ON si.codigo_reparticion = REP.id_reparticion 
                INNER JOIN CO_GED.CARGOS CAR ON d.CARGO = CAR.ID`;


        connection = await oracledb.getPool(ambiente).getConnection();

        let res = await connection.execute(query);

        let usuarios = res.rows.map(tupla => {
            const user = {
                nombre_apellido: tupla[1] + " " + tupla[2],
                usuario: tupla[0],
                nombre: tupla[1],
                apellido: tupla[2],
                cuit: tupla[3],
                reparticion: tupla[4],
                email:tupla[5],
                sector:tupla[6],
                sello:tupla[7],
                activo: Boolean(tupla[8])
            }
            redisClient.set(`${EVENTOS.usuarios}:${ambiente}:${user.usuario}`, JSON.stringify(user))
            return user
        })

        redisClient.set(EVENTOS.usuarios + ":" + ambiente, JSON.stringify(usuarios));
        return usuarios;
    } catch (error) {
        throw error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw err;
            }
        }
    }
}

export default actualizarUsuarios
