import oracledb from 'oracledb';
import HttpException from '../exceptions/HttpExceptions';


const TABLA = "EE_GED.EE_AUDITORIA_DE_CONSULTA";
const COLUMNAS = {
    TIPO_ACTUACION: TABLA + ".TIPO_ACTUACION",
    ANIO_ACTUACION: TABLA + ".ANIO_ACTUACION",
    NUMERO_ACTUACION: TABLA + ".NUMERO_ACTUACION",
    REPARTICION_ACTUACION: TABLA + ".REPARTICION_ACTUACION",
    REPARTICION_USUARIO: TABLA + ".REPARTICION_USUARIO",
    FECHA_CONSULTA: TABLA + ".FECHA_CONSULTA",
    USUARIO: TABLA + ".USUARIO"
}
async function obtenerAuditorias({
    fechaDesde = "01/01/2021",
    fechaHasta,
    usuario,
    reparticionUsuario,
    numeroExpediente,
    anioExpediente,
    ambiente = "prod"
}) {
    let connection = null;
    try {
        let pars = { fechaDesde };
        if (fechaHasta)
            pars = { ...pars, fechaHasta };
        const stringFechaHasta = fechaHasta ? "TO_DATE(:fechaHasta, 'DD/MM/YYYY' ) + 1 " : "SYSDATE";
        let whereClausure = `WHERE ${COLUMNAS.FECHA_CONSULTA} BETWEEN (TO_DATE(:fechaDesde,'DD/MM/YYYY') ) and ${stringFechaHasta}`;

        if (anioExpediente) {
            whereClausure += ` AND ${COLUMNAS.ANIO_ACTUACION} = :anioExpediente `;
            pars = { ...pars, anioExpediente };
        }

        if (numeroExpediente) {
            whereClausure += ` AND ${COLUMNAS.NUMERO_ACTUACION} = :numeroExpediente `;
            pars = { ...pars, numeroExpediente };
        }


        if (reparticionUsuario) {
            whereClausure += ` AND ${COLUMNAS.REPARTICION_USUARIO} = :reparticionUsuario `;
            pars = { ...pars, reparticionUsuario };
        }

        if (usuario) {
            whereClausure += ` AND ${COLUMNAS.USUARIO} = :usuario `;
            pars = { ...pars, usuario };
        }

        const query = `--sql
            SELECT 
                ${COLUMNAS.TIPO_ACTUACION}, 
                ${COLUMNAS.ANIO_ACTUACION},
                ${COLUMNAS.NUMERO_ACTUACION}, 
                ${COLUMNAS.REPARTICION_ACTUACION}, 
                ${COLUMNAS.REPARTICION_USUARIO}, 
                ${COLUMNAS.USUARIO}, 
                ${COLUMNAS.FECHA_CONSULTA} 
            FROM ${TABLA}
        
            ${whereClausure}
            ORDER BY ${COLUMNAS.FECHA_CONSULTA} DESC
            FETCH FIRST 300 ROWS ONLY`;
        let resp = [];
        connection = await oracledb.getPool(ambiente).getConnection();

        //  console.log(connection);
        let res = await connection.execute(query, pars);
        if (res.rows.length < 1) {
            throw new HttpException( new Error("No hay datos"), 404);
        }


        resp = res.rows.map(e => {
            const numeroExp = completaZero("00000000" +  e[2], 8);
            const expediente = e[0] + '-' + e[1] + '-' + numeroExp + '--' + e[3] + '-' + e[4]; 
            return {
                nroSade:expediente,
                usuario:e[5],
                fechaConsulta:e[6]
            }
        })
        return resp;
    } catch (error) {
        console.log(error);
        throw error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.log({ err });
            }
        }
    }
}

/**
 *  * @param {object} datosExpedientes tupla de un expediente
 */
function generarNumeroExpediente(datosExpedientes){
    const numeroExp = completaZero("00000000" + datosExpedientes.NUMERO, 8);
    let expediente = datosExpedientes.TIPO_DOCUMENTO + '-' + datosExpedientes.ANIO + '-' + numeroExp + '--' + datosExpedientes.CODIGO_REPARTICION_ACTUACION + '-' + datosExpedientes.CODIGO_REPARTICION_USUARIO;
    return expediente;
}

function completaZero(str, chr){
    return str.substr(str.length - chr, str.length)
}

export default obtenerAuditorias;
