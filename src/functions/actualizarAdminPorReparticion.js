import oracledb from 'oracledb';
import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';


async function actualizarAdminPorReparticion(ambiente="prod") {
    let connection = null;
    try {
        const query = `SELECT AD.NOMBRE_USUARIO ,REP.CODIGO_REPARTICION,REP.NOMBRE_REPARTICION,DU.APELLIDO_NOMBRE
            FROM TRACK_GED.sade_admin_reparticion AD 
               JOIN TRACK_GED.SADE_REPARTICION REP ON AD.ID_REPARTICION = REP.ID_REPARTICION
               JOIN CO_GED.DATOS_USUARIO DU ON DU.usuario = AD.nombre_usuario `;

        connection = await oracledb.getPool(ambiente).getConnection();
        let res = await connection.execute(query);
        let usuarios = [];
        res.rows.forEach(tupla => {
            let usuario = usuarios.find(e => e.nombreUsuario == tupla[0]);
            if (usuario) {
                usuario.reparticiones.push({ codigo: tupla[1], nombre: tupla[2] });
            } else {
                usuarios.push(
                    {
                        nombreUsuario: tupla[0],
                        apellidoNombre: tupla[3],
                        reparticiones: [{ codigo: tupla[1], nombre: tupla[2] }]
                    });
            }
        });
        
        redisClient.set(EVENTOS.adminPorReparticion, JSON.stringify(usuarios));
        return usuarios;
    } catch (error) {
        throw  error ;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw  err ;
            }
        }
    }
}

export default actualizarAdminPorReparticion;


