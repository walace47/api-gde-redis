import { consultarUsuarioDNI } from '../functions'

export const obtenerUsuarioConDNI = async (req,res,next) => {
    try {
        const { dni } = req.params;
        const {ambiente} = req.query;
        const respuesta = await consultarUsuarioDNI({dni,ambiente});
        res.json(respuesta);
    } catch (error) {
        next(error)
    }
}
