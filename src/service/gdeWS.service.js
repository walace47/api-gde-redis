import {axiosIntance, user} from '../config/config.js';
import HttpException from '../exceptions/HttpExceptions.js';

async function login({ambiente} ){
    try{
        return (await axiosIntance.post(process.env.LOGIN_GDE + '/'+ ambiente,user)).data
    }catch(e){
        console.error("Error con LOGEARSE CON EL SERVIDOR usuario: " + user.username)
        console.error(e);
        throw new HttpException(new Error("error con conectarse al servidor"),500)
    } 
    
}

/**
 * 
 * @param {{nroSade:string, ambiente:string, jwt:string}} param0 
 * @returns {Promise<import("axios").AxiosResponse<{return:string},any>>}
 */
export async function descargarGEDO({nroSade,ambiente,authorizationHeader}){
    let jwt = 'Bearer '
    if(!authorizationHeader) {
        jwt+= await login({ambiente});
    }else{
        jwt = authorizationHeader;
    }

    let url = process.env.URL_DESCARGAR_GDE + '/'+ encodeURIComponent(nroSade) + '/' + ambiente;
    return axiosIntance.get(url,
        {headers:{'Authorization': authorizationHeader}})
}