/**
 * 
 * @param {string} token 
 * @returns {GDEToken} 
 *          Datos del token de GDE 
 */
export function parseJwt(token) {
    return JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
}


/**
 * @typedef {import('../config/types').GDEToken} GDEToken 
*/

/**
 *
 *
 * @export
 * @param {string} element
 * @param {string[]} array
 */
export function nestedInclude(element, array) {
    let res = false;
    let i = 0;

    while ( !res && i < array.length) {
        res = array[i].includes(element)
        i++;
    }
    return res;   

}
